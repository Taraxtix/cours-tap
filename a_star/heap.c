#include "heap.h"

#include <stdio.h>
#include <stdlib.h>

#define ASSERT(x)                                    \
  do {                                               \
    if (!(x)) {                                      \
      fprintf(stderr, "Assertion failed: %s\n", #x); \
      exit(1);                                       \
    }                                                \
  } while (0)
#define SWAP(x, y)     \
  do {                 \
    void *__tmp__ = x; \
    x = y;             \
    y = __tmp__;       \
  } while (0)
#define LEFT(i) (i * 2)
#define RIGHT(i) ((i * 2) + 1)
#define PARENT(i) (i / 2)

heap heap_create(int k, int (*f)(const void *, const void *)) {
  heap h = malloc(sizeof(*h));
  ASSERT(h != NULL);
  h->array = malloc(sizeof(void *) * (k + 1));
  ASSERT(h->array != NULL);
  h->n = 0;
  h->nmax = k;
  h->f = f;

  return h;
}

void heap_destroy(heap h) {
  free(h->array);
  h->array = NULL;
  h->n = 0;
  h->nmax = 0;
  h->f = NULL;
  free(h);
  h = NULL;
}

bool heap_empty(heap h) { return h->n == 0; }

bool heap_add(heap h, void *object) {
  if (h->n == h->nmax) {
    h->nmax *= 2;
    h->array = realloc(h->array, sizeof(void *) * (h->nmax + 1));
    ASSERT(h->array != NULL);
  };
  h->n++;
  int idx = h->n;
  h->array[idx] = object;
  while (idx != 1 && h->f(h->array[idx], h->array[PARENT(idx)]) < 0) {
    SWAP(h->array[idx], h->array[PARENT(idx)]);
    idx = PARENT(idx);
  }
  return false;
}

void *heap_top(heap h) {
  if (heap_empty(h)) return NULL;
  return h->array[1];
}

void *heap_tail(heap h) {
  if (heap_empty(h)) return NULL;
  return h->array[h->n--];
}

void *heap_pop(heap h) {
  void *out = heap_top(h);

  if (h->n-- <= 0) return out;

  h->array[1] = h->array[h->n];
  int idx = 1;
  while (LEFT(idx) <= h->n) {
    int smaller = LEFT(idx);
    if (RIGHT(idx) <= h->n && h->f(h->array[RIGHT(idx)], h->array[smaller]) < 0) {
      smaller = RIGHT(idx);
    }
    if (h->f(h->array[smaller], h->array[idx]) >= 0) break;
    SWAP(h->array[smaller], h->array[idx]);
    idx = smaller;
  }
  return out;
}
