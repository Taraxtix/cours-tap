#include "tools.h"

//
//  TSP - BRUTE-FORCE
//
// -> la structure "point" est définie dans "tools.h"
// -> tsp_main peut être testé dès les 3 premières fonctions codées
//

double dist(point A, point B) {
  return hypot(A.x - B.x, A.y - B.y);
}

double value(point *V, int n, int *P) {
  double sum = dist(V[P[n - 1]], V[P[0]]);
  for (int i = 0; i < n - 1; i++){
    sum += dist(V[P[i]], V[P[i + 1]]);
  }
  return sum;
}

double tsp_brute_force(point *V, int n, int *Q) {
  int Q_copy[n];
  for (size_t i = 0; i < n; i++) {Q_copy[i] = i; Q[i] = i;}
  double min = value(V, n, Q_copy);
  
  while(NextPermutation(Q_copy, n) && running) {
    double val = value(V, n, Q_copy);
    if (val < min){
      min = val;
      memcpy(Q, Q_copy, sizeof(int) * n);  
      drawTour(V,n,Q);
    }
  }
  return min;
}

void MaxPermutation(int *P, int n, int k) {
  if (k >= n-1) return;
  int placed = 0;
  bool available[n]; memset(available, 0, sizeof(bool) * n);
  
  for (int i = k; i < n; i++) available[P[i]] = true;
  for (int i = n-1; i >= 0; i--) {
    if (available[i]) P[k + (placed++)] = i;
  }
}

double value_opt(point *V, int n, int *P, double w) {
  static double **D = NULL;
  if (!D) {
    D = malloc(sizeof(double*) * n);
    for (int i = 0; i < n; i++){
      D[i] = malloc(sizeof(double) * n);
      for (int j = 0; j < n; j++) D[i][j] = dist(V[i], V[j]);
    }
  }

  double sum = D[P[n - 1]][P[0]];
  for (int i = 0; i < n - 1; i++){
    if(sum + D[P[i]][P[n-1]] >= w) return -(double)(i + 2);
    sum += D[P[i]][P[i+1]];
  }
  return sum;
}

double tsp_brute_force_opt(point *V, int n, int *Q) {
  int Q_copy[n];
  for (size_t i = 0; i < n; i++) {Q_copy[i] = i; Q[i] = i;};
  double min = value(V, n, Q_copy);

  while(NextPermutation(Q_copy, n) && running) {
    if (Q_copy[0] != 0) break;
    double val = value_opt(V, n, Q_copy, min);
    if (val < 0) MaxPermutation(Q_copy, n, -val);
    else{
      min = val;
      memcpy(Q, Q_copy, sizeof(int) * n);
      drawTour(V,n,Q);
    }
  }
  return min;
}
