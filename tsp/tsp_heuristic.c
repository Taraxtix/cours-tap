#include "tools.h"
#include "tsp_brute_force.h"

//
//  TSP - HEURISTIQUES
//

void reverse(int *T, int p, int q)
{
  // Renverse la partie T[p]...T[q] du tableau T avec p<q si
  // T={0,1,2,3,4,5,6} et p=2 et q=5, alors le nouveau tableau T sera
  // {0,1, 5,4,3,2, 6}.
  int tmp;
  for (int i = 0; i < (q - p + 1) / 2; i++)
  {
    tmp = T[p + i];
    T[p + i] = T[q - i];
    T[q - i] = tmp;
  }
}

double first_flip_on_intersection(point *V, int n, int *P)
{
  // Renvoie le gain>0 du premier flip réalisable, tout en réalisant
  // le flip, et 0 s'il n'y en a pas.

  for (int i = 0; i < n - 2; i++)
  {
    for (int j = i + 2; j < n; j++)
    {
      if (i == 0 && j == n - 1)
        continue;

      point p1 = V[P[i]].x <= V[P[i + 1]].x ? V[P[i]] : V[P[i + 1]];
      point p2 = V[P[i]].x <= V[P[i + 1]].x ? V[P[i + 1]] : V[P[i]];
      point p3;
      point p4;
      if (j == n - 1)
      {
        p3 = V[P[j]].x <= V[P[0]].x ? V[P[j]] : V[P[0]];
        p4 = V[P[j]].x <= V[P[0]].x ? V[P[0]] : V[P[j]];
      }
      else
      {
        p3 = V[P[j]].x <= V[P[j + 1]].x ? V[P[j]] : V[P[j + 1]];
        p4 = V[P[j]].x <= V[P[j + 1]].x ? V[P[j + 1]] : V[P[j]];
      }
      double fst_coef = (p2.y - p1.y) / (p2.x - p1.x);
      double fst_offset = p1.y - fst_coef * p1.x;
      double snd_coef = (p4.y - p3.y) / (p4.x - p3.x);
      double snd_offset = p3.y - snd_coef * p3.x;
      double intersection_x = (snd_offset - fst_offset) / (fst_coef - snd_coef);
      if (intersection_x >= p1.x && intersection_x <= p2.x && intersection_x >= p3.x && intersection_x <= p4.x)
      {
        double prev_value = value(V, n, P);
        reverse(P, i + 1, j);
        return prev_value - value(V, n, P);
      }
    }
  }
  return 0.0;
}

double first_flip(point *V, int n, int *P)
{
  double prev_val = value(V, n, P);
  double new_val;
  int Q[n];
  memcpy(Q, P, sizeof(int) * n);

  for (int i = 0; i < n - 2; i++)
  {
    for (int j = i + 2; j < n; j++)
    {
      reverse(Q, i + 1, j);
      if ((new_val = value(V, n, Q)) < prev_val)
      {
        memcpy(P, Q, sizeof(int) * n);
        return prev_val - new_val;
      }

      memcpy(Q, P, sizeof(int) * n);
    }
  }

  return 0.0;
}

double tsp_flip(point *V, int n, int *P)
{
  for (int i = 0; i < n; i++)
    P[i] = i;
  double gain = 0.;
  while ((gain = first_flip(V, n, P)) != 0.0 && running)
    drawTour(V, n, P);

  return value(V, n, P);
}

double tsp_greedy(point *V, int n, int *P)
{
  // La fonction doit renvoyer la valeur de la tournée obtenue. Pensez
  // à initialiser P, par exemple à P[i]=i.

  bool freePoints[n];
  for (int i = 0; i < n; i++)
  {
    freePoints[i] = true;
  }
  P[0] = 0;
  freePoints[0] = false;

  for (int i = 1; i < n; i++)
  {
    int min_index = 0;
    double min = DBL_MAX;
    for (int j = 1; j < n; j++)
    {
      if (freePoints[j] && dist(V[i - 1], V[j]) < min)
      {
        min = dist(V[i - 1], V[j]);
        min_index = j;
      }
    }
    P[i] = min_index;
    freePoints[min_index] = false;
    if (!running)
    {
      return -1.0;
    }
  }
  return value(V, n, P);
}
